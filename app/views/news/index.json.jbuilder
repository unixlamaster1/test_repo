json.array!(@news) do |news|
  json.extract! news, :id, :params
  json.url news_url(news, format: :json)
end
