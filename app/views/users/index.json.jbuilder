json.array!(@users) do |user|
  json.extract! user, :id, :LastName, :group_id
  json.url user_url(user, format: :json)
end
